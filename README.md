# Mangrove

Mangrove is a nodal pipeline manager. It stores and manage the projects data, creates connected nodes representing tasks and help you with versionning and contextuals actions. And so much more !

## Getting Started

This software is written in python.

Download it from gitlab or install it with "pip install mgv".

You can test Mangrove without a database manager, but using mongodb is highly recommended.

### Prerequisites

Python2.7 to 3.7

### Installing

Install python from https://www.python.org/downloads

Installing a MongoDB server : https://www.mongodb.com/download-center/community

## Authors

* **Bekri Djelloul** - *Initial work*

## Copyright and License

Distributed under the GNU License - LGPLv3

see the [LICENSE](LICENSE) file for details

© 2019 THE YARD VFX. All rights reserved.